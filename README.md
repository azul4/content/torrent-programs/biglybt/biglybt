# biglybt

BiglyBT, a feature filled, open source, ad-free, bittorrent client. BiglyBT is forked from the original project and is being maintained by two of the original developers as well as members of the community.

https://github.com/BiglySoftware/BiglyBT

<br><br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/torrent-programs/biglybt/biglybt.git
```

